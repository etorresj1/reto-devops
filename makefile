image-name="node-api"

build:
	docker build -t $(image-name) . 

run:
	docker run -p 3000:3000 -d $(image-name)
